package tfs.naval.ship

import tfs.naval.model.Ship

class MockShipController extends ShipController {

  override def validateShip(ship: Ship): Boolean = true
}