package tfs.naval.ship

import tfs.naval.model.Ship

class RealShipController extends ShipController {

  override def validateShip(ship: Ship): Boolean = {
    if(ship.size > 4 || ship.isEmpty)
      false
    else if((ship.count(_._1 == ship.head._1) == ship.size) || ship.count(_._2 == ship.head._2) == ship.size)// проверяем, что все точки находятся в одной строке(столбце)
      true
    else
      false
  }
}
