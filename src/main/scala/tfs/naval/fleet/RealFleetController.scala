package tfs.naval.fleet

import tfs.naval.model.{Fleet, Ship}

class RealFleetController extends FleetController {

  override def enrichFleet(fleet: Fleet, name: String, ship: Ship): Fleet = {
    fleet + (name -> ship)
  }
}
