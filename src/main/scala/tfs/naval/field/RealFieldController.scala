package tfs.naval.field

import tfs.naval.model.{Field, Ship}

class RealFieldController extends FieldController {

  override def validatePosition(ship: Ship, field: Field): Boolean = {
    val (xs, ys) = (ship.map(point => point._1), ship.map(point => point._2))//выборка из значений x и y всех точек ship
    val (xMax, xMin) = (xs.max, xs.min)//минимальное и максимальное значение x
    var (yMax, yMin) = (ys.max, ys.min)//минимальное и максимальное значение y
    var result: Boolean = true
    //проходим по потенциальным точкам нового корабля и проверяем, что в данных точках и соседних не стоят корабли
    for(x <- (if(xMin != 0) xMin-1 else xMin) to (if(xMax != field.length-1) xMax+1 else xMax);
        y <- (if(yMin != 0) yMin-1 else yMin) to (if(yMax != field.length-1) yMax+1 else yMax)){
      if(field(x)(y))//если стоит корабль
        result = false
    }
    result
  }

  override def markUsedCells(field: Field, ship: Ship): Field = {
    var field2 = identity(field)
    ship.foreach(point => field2 = field2.updated(point._1, field2(point._1).updated(point._2, true)))
    field2
  }
}
