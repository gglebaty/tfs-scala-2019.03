package tfs.naval

import tfs.naval.field.RealFieldController
import tfs.naval.fleet.RealFleetController
import tfs.naval.io.FixedShipsReader
import tfs.naval.model.Ship
import tfs.naval.ship.RealShipController

object Main {
  def main(args: Array[String]): Unit = {
    val shipsReader = new FixedShipsReader

    /*val shipController = new MockShipController
    val fleetController = new MockFleetController
    val fieldController = new MockFieldController*/

    val shipController = new RealShipController
    val fleetController = new RealFleetController
    val fieldController = new RealFieldController

    val solution  = new Solution(shipController, fieldController, fleetController)

    val initField = Vector.fill(10){
      Vector.fill(10)(false)
    }

    val result = shipsReader.read.foldLeft(initField -> Map.empty[String, Ship]) {
      case (state, (name, ship: Ship)) => solution.tryAddShip(state, name, ship)
    }

    for(i <- result._2){
      println(i._1)
    }
  }
}

