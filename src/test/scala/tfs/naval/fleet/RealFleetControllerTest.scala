package tfs.naval.fleet

import org.scalatest.{FlatSpec, Matchers}
import tfs.naval.model.{Point, Ship}

class RealFleetControllerTest extends FlatSpec with Matchers {

  behavior of "RealFleetControllerTest"

  it should "enrichFleet" in {
    val fleet = Map("Ship1" -> List(new Point(7,6), new Point(7,7)))
    val ship2 = List(
      (1, 2),
      (1, 3)
    )
    println(ship2.size)
    val fleet2 = (new RealFleetController).enrichFleet(fleet, "Ship2", ship2)

    require(fleet2.size == 2)

    require(fleet2.keys.toList.contains("Ship2"))

    require(fleet2.getOrElse("Ship2", Nil).size == 2)
  }

}
