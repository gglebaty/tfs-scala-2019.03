package tfs.naval.field

import org.scalatest.{FlatSpec, Matchers}
import tfs.naval.Solution

class RealFieldControllerTest extends FlatSpec with Matchers {

  behavior of "RealFieldControllerTest"

  it should "validatePosition" in {
    val field = Vector(
      Vector(false, false, false, false, false),
      Vector(false, true, false, false, false),
      Vector(false, true, false, false, false),
      Vector(false, false, false, false, false),
      Vector(false, false, false, false, false)
    )
    val ship = List(
      (0, 0)
    )
    (new RealFieldController).validatePosition(ship, field) shouldBe false

    val ship2 = List(
      (1, 2),
      (1, 3),
      (1, 4)
    )
    (new RealFieldController).validatePosition(ship2, field) shouldBe false
  }

  it should "markUsedCells" in {
    val field = Vector(
      Vector(false, false, false, false, false),
      Vector(false, false, false, false, false),
      Vector(false, false, false, false, false),
      Vector(false, false, false, false, false),
      Vector(false, false, false, false, false)
    )
    val ship = List(
      (1, 2),
      (1, 3),
      (1, 4)
    )
    (new RealFieldController).markUsedCells(field, ship)
  }

}
