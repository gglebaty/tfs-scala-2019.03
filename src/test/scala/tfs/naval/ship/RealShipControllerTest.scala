package tfs.naval.ship

import org.scalatest.{FlatSpec, Matchers}
import tfs.naval.Solution

class RealShipControllerTest extends FlatSpec with Matchers {

  behavior of "RealShipControllerTest"

  "ships".should(right = "be linear").in {
    val ship = List(
      (1,1),
      (2,1),
      (3,1)
    )
    (new RealShipController).validateShip(ship) shouldBe true
  }

  "ship with one cell" should "be correct" in {
    val ship = List((3,3))
    (new RealShipController).validateShip(ship) shouldBe true
  }

  "empty ship" should "be invalid" in{
    val ship = List()
    (new RealShipController).validateShip(ship) shouldBe false
  }
  "ships" should "be no longer then 4" in {
    val ship5 = List(
      (1, 3),
      (1, 4),
      (1, 5),
      (1, 6),
      (1, 7)
    )
    (new RealShipController).validateShip(ship5) shouldBe false
  }

}
